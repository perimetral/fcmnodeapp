//	here is configuration of overall app
//	please keep values being type compatible

const cfg = {};


	//	affects Express behavior
	cfg.app = {};

//	string; may be one of: 'production', 'development'
//	defines app environment mode
cfg.app.env = process.env.NODE_ENV || 'production';

//	boolean
//	whether to treat '/FILE.IMG' and '/file.img' as different routes
cfg.app['case sensitive routing'] = false;

//	boolean
//	whether to treat '/path' and '/path/' as different routes
cfg.app['strict routing'] = false;

//	boolean
//	removes boilerplate Express header
cfg.app['x-powered-by'] = false;

//	boolean or string; may be one of: false, 'simple', 'extended'
//	disables automatic query parser
cfg.app['query parser'] = false;


	//	affects routing
	cfg.app.gate = {};

//	string; may be one of: 'checkout', 'copy', 'delete', 'get', 'head', 'lock', 'merge',
//		'mkcol', 'move', 'm-search', 'notify', 'options', 'patch', 'post', 'purge',
//		'put', 'report', 'search', 'subscribe', 'trace', 'unlock', 'unsubscribe', 'all'
//	defines REST method to use
cfg.app.gate.method = 'all';

//	string
//	where to listen for data
cfg.app.gate.path = '/';


	//	Firebase service data
	cfg.firebase = {};

//	string
//	represents correct Firebase API key path
cfg.firebase.key = require('path').join(__dirname, 'fcmnodeapp-firebase-adminsdk-3nsp4-1fdc27b2ca.json');

//	string
//	if empty, FCM uses notification ID as collapse key
//	if not, forces FCM to use this collapse key instead of notification ID
cfg.firebase.collapseKey = '';


	//	affects server behavior
	cfg.server = {};

//	string; may be formatted as IPv4, IPv6 or URI
//	hostname to listen at
cfg.server.hostname = '127.0.0.1';

//	number; must be in range 1025..65535
//	port to listen at
cfg.server.port = 3005;


	//	determines MongoDB specifics
	cfg.mongo = {};

//	string
//	MongoDB connection path
cfg.mongo.uri = 'mongodb://127.0.0.1:27017';

//	string
//	MongoDB database name
cfg.mongo.dbName = 'atata';

//	string
//	MongoDB collection name
cfg.mongo.collection = 'ololo';

//	string
//	MongoDB key for looking UID up
cfg.mongo.UIDKey = 'uid';

//	string
//	MongoDB key for projecting of user_token data
cfg.mongo.userTokenKey = 'userToken';

	//	determines how to work with templates
	cfg.templates = {};

//	async function
//	used to initially load templates
cfg.templates.load = async () => {
	return require('./templates.json');
};


	//	determines clustering strategy
	cfg.workers = {};

//	number; must be positive
//	how much of workers to produce
cfg.workers.count = require('os').cpus().length;

//	string; must present compliant kill signal
//	what signal to send to dying worker
cfg.workers.killSignal = 'SIGTERM';


	//	affects input data checks
	cfg.input = {};

//	object
//	used for incoming data checks
//	keys represent required fields, and are linked to their async check functions accordingly
cfg.input.uid = {
	item: 'uid',
	prover: async (x) => {
		return !! x;
	},
};
cfg.input.template_id = {
	item: 'template_id',
	prover: async (x) => {
		if (typeof x !== typeof '') return false;
		return !! x.length;
	},
};
cfg.input.template_values = {
	item: 'template_values',
	prover: async (x) => {
		if (typeof x === typeof '') {
			try {
				return JSON.parse(x);
			} catch (e) {
				return false;
			};
		} else return typeof x === typeof {};
	},
};
cfg.input.notification_id = {
	item: 'notification_id',
	prover: async (x) => {
		if (typeof x !== typeof '') return false;
		return !! x.length;
	},
};
cfg.input.time = {
	item: 'time',
	prover: async (x) => {
		return true;
	},
};


	//	affects JSON middleware
	cfg.ejson = {};

//	boolean
//	process compressed bodies
cfg.ejson.inflate = true;

//	boolean
//	limits JSON parser by arrays and objects
cfg.ejson.strict = false;

//	number; must be positive
//	JSON body size limit
cfg.ejson.limit = 1024 * 1024;

//	sync function; must return parsed value
//	parser calls this for each key/value pair
cfg.ejson.reviver = (key, value) => {
	return value;
};

	//	defines security policies
	cfg.helmet = {};

//	boolean
//	protects users from clickjacking
//	disabled due to having no valuability in this app
cfg.helmet.frameguard = false;

//	boolean
//	whether to modify headers for IE
//	disabled due to having no valuability in this app
cfg.helmet.ieNoOpen = false;


	//	forces HTTPS to users
	cfg.helmet.hsts = {};

//	sync function; must return boolean
//	turns HSTS on/off, depending on returned value
cfg.helmet.hsts.setIf = (req, res) => {
	return !! req.secure;
};

//	number; must be positive; treated as seconds
//	blocks insecure HTTP connections for provided period of time
cfg.helmet.hsts.maxAge = 60 * 60 * 24 * 42;

//	boolean
//	populates HSTS to subdomains
cfg.helmet.hsts.includeSubDomains = true;

//	boolean
//	preloads HSTS policy to user before link setup
cfg.helmet.hsts.preload = true;

//	function
//	provides logging
cfg.logger = console.log;

module.exports = cfg;