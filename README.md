# fcmnodeapp

1. Install it directly by fetching sources: `git clone https://perimetral@bitbucket.org/perimetral/fcmnodeapp.git`
2. Install and configure latest versions of Node.js and MongoDB
3. Run MongoDB
4. Go to fetched directory: `cd fcmnodeapp`
5. Install dependencies: `npm i`
6. Configure app according to your needs by editing *config.js* - it is populated by detailed comments, so follow ones to know what do settings mean
7. Run it directly by `node index.js` or indirectly by `npm run start`