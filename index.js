global.cfg = require('./config');
global.log = cfg.logger;

const util = require('util');
const cluster = require('cluster');
const express = require('express');
const helmet = require('helmet');
const mongo = require('mongodb').MongoClient;
const fcmPush = require('fcm-push');
const bodyParser = require('body-parser');

const checkInput = async (data) => {
	if (typeof(data) !== typeof({})) return false;
	for (let i in cfg.input) {
		if (! cfg.input[i].item in data) {
			log(`ERR: ${cfg.input[i].item}`);
			return false;
		};
		if (! (await cfg.input[i].prover(data[i]))) {
			log(`ERRD: ${i}`);
			return false;
		};
	};
	return true;
};

const connectMongo = async () => {
	return new Promise((go, stop) => {
		mongo.connect(cfg.mongo.uri, (e, client) => {
			if (e) return stop(e);
			var db = client.db(cfg.mongo.dbName);
			db.closeClient = client.close;
			return go(db);
		});
	});
};

const impl = async () => {
	var fcm = new fcmPush(cfg.firebase.key);

	let db = await connectMongo();
	let users = db.collection(cfg.mongo.collection);
	let templates = await cfg.templates.load();

	var app = express();

	app.set('env', cfg.app.env);
	app.set('case sensitive routing', cfg.app['case sensitive routing']);
	app.set('strict routing', cfg.app['strict routing']);
	app.set('strict routing', cfg.app['strict routing']);
	app.set('x-powered-by', cfg.app['x-powered-by']);
	app.set('query parser', cfg.app['query parser']);

	app.use(helmet(cfg.helmet));
	app.use(express.json(cfg.jsonMiddleware));
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());

	app[cfg.app.gate.method](cfg.app.gate.path, async (req, res, next) => {
		let passed = await checkInput(req.body);
		if (! passed) {
			return res.end('RECIEVED DATA IS INVALID');
		};
		let uid = req.body[cfg.input.uid.item];
		let templateId = req.body[cfg.input.template_id.item];
		let templateValues = req.body[cfg.input.template_values.item];
		let notificationId = req.body[cfg.input.notification_id.item];
		let time = req.body[cfg.input.time.item];
		let token = (await users.find({
			[cfg.mongo.UIDKey]: uid
		}).project({
			[cfg.mongo.userTokenKey]: 1
		}).limit(1).toArray())[0];

		if (! (templateId in templates)) {
			let e = new Error(`NO TEMPLATE WITH ID ${templateId} FOUND`);
			throw e;
		};
		let result = templates[templateId];
		for (let i in templateValues) result = result.split(i).join(templateValues[i]);
		let msg = {
			to: token,
			collapse_key: cfg.firebase.collapseKey.length ? cfg.firebase.collapseKey : notificationId,
			data: {
				time,
				notification_id: notificationId,
			},
			notification: {
				title: templateId,
				body: result,
			},
		};
		if (typeof time === typeof '') time = Number(time);
		let now = new Date().getTime();
		if (time <= now) {
			try {
				let response = await fcm.send(msg);
				log(`SENT NOTIFICATION ${notificationId} TO UID ${uid}: ${response}`);
			} catch (e) {
				log(`NOTIFICATION ${notificationId} SENDING FAILED WITH: ${util.inspect(e)}`);
			};
		} else {
			setTimeout((opts) => {
				opts.fcm.send(opts.msg).then((response) => {
					log(`SENT NOTIFICATION ${opts.notificationId} TO UID ${opts.uid}: ${response}`);
				}).catch((e) => {
					log(`NOTIFICATION ${opts.notificationId} SENDING FAILED WITH: ${util.inspect(e)}`);
				});
			}, time - now, {
				fcm,
				msg,
				notificationId,
				uid,
			});
		};
	});

	app.listen(cfg.server.port, cfg.server.hostname, (e) => {
		if (e) {
			log(`ERR WITH PID ${process.pid}: ${util.inspect(e)}`);
			db.closeClient();
			process.exit(e.code);
		};
		log(`PID ${process.pid} LISTENS AT ${cfg.server.hostname}:${cfg.server.port}`);
	});
};

if (cluster.isMaster) {
	log('SPAWNING WORKERS...');
	const doFork = () => {
		let fork = cluster.fork(process.env);
		fork.on('error', (e) => {
			log(e);
			try {
				fork.kill(cfg.workers.killSignal);
			} catch (e) {};
		});
		return fork;
	};
	cluster.on('exit', (worker, code, signal) => {
		log(`WORKER DIED. CODE: ${code}. PID: ${worker.process.pid}. SIGNAL: ${signal}`);
		doFork();
	});
	for (let i = 0; i < cfg.workers.count; i++) doFork();
} else if (cluster.isWorker) {
	impl().then(() => {
	}).catch((e) => {
		log(`IN-APP ERROR: ${util.inspect(e)}`);
	});
};